from .gitlab import GitlabAccess
from .rclone import RCloneAccess
from .sftp import SFTPAccess

__all__ = ['GitlabAccess', 'SFTPAccess', 'RCloneAccess']
